import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxCommunicationComponent } from './ngx-communication.component';

describe('NgxCommunicationComponent', () => {
  let component: NgxCommunicationComponent;
  let fixture: ComponentFixture<NgxCommunicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxCommunicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
