import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgxCommunicationComponent } from './ngx-communication/ngx-communication.component';

@NgModule({
  declarations: [
    AppComponent,
    NgxCommunicationComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
